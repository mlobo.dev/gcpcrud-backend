package com.dev4abyss.crudgcp.controller;

import com.dev4abyss.crudgcp.model.Person;
import com.dev4abyss.crudgcp.service.PersonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@Tag(name = "Persons")
@RequestMapping("/persons")
public class SheetController {

    private final PersonService service;

    @PostMapping
    @Operation(summary = "Saves a new person ")
    public ResponseEntity<Void> save(@RequestBody Person person) {
        service.save(person);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping
    @Operation(summary = "Find all Persons")
    public ResponseEntity<List<Person>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

}