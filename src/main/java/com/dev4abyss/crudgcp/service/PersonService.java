package com.dev4abyss.crudgcp.service;

import com.dev4abyss.crudgcp.model.Person;
import com.dev4abyss.crudgcp.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PersonService {

    private final PersonRepository repository;


    public void save(Person person) {
        repository.save(person);
    }

    public List<Person> findAll() {
        return repository.findAllOrderByName();
    }
}
