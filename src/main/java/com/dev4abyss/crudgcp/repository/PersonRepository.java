package com.dev4abyss.crudgcp.repository;

import com.dev4abyss.crudgcp.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository  extends JpaRepository<Person,Long> {
    @Query("select p from Person p order by p.name asc")
    List<Person> findAllOrderByName();

}
